"""
This is the implementation of data preparation for sklearn
"""

import logging
import sys
import os
import pandas as pd
import numpy as np
# Following two imports are required for Xpresso. Do not remove this.
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger

__author__ = "### Author ###"

# To use the logger please provide the name and log level
#   - name is passed as the project name while generating the logs
#   - level can be DEBUG, INFO, WARNING, ERROR, CRITICAL
logger = XprLogger(name="xgboost_data_prep",
                   level=logging.INFO)


class XgboostDataPrep(AbstractPipelineComponent):
    """
    Core implementation of data preparation required for
      store sales
    Main class for any pipeline job. It is extended from AbstractPipelineComponent
    which allows xpresso platform to track and manage the pipeline.
    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.
    """

    def __init__(self, train_file_path, test_file_path, store_file_path):
        super().__init__(name="XgboostDataPrep")
        self.train_data = pd.read_csv(train_file_path)
        self.store_data = pd.read_csv(store_file_path)
        self.test_data = pd.read_csv(test_file_path)

        # Merging Store file into a single dataset for train and test
        self.combined_train_data = None
        self.combined_test_data = None
        logging.info("Data load completed")

    def prepare_date(self, data):
        data['Date'] = pd.to_datetime(data['Date'], errors='coerce')
        data['Date'] = data['Date'].dt.strftime('%d-%m-%Y')
        data['Year'] = pd.DatetimeIndex(data['Date']).year
        data['Month'] = pd.DatetimeIndex(data['Date']).month
        data['Date'] = pd.to_datetime(data['Date'])
        data['DayOfWeek'] = data['Date'].dt.weekday_name

    # Python Function to remove outliers
    def remove_outliers(self, df, column, min_val, max_val):
        col_values = df[column].values
        df[column] = np.where(
            np.logical_or(col_values <= min_val, col_values >= max_val), np.NaN,
            col_values)
        return df

    def start(self, run_name):
        """
        This is the start method, which does the actual data preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the Controller that
              the component has started processing (details such as the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            run_name: xpresso run name which is used by base class to identify
               the current run. It must be passed. While running as pipeline,
               Xpresso automatically adds it.

        """
        try:
            super().start(xpresso_run_name=run_name)
            logging.info("Preparing data")
            self.combined_train_data = self.preprocess(data=self.train_data)
            self.combined_test_data = self.preprocess(data=self.test_data)
        except Exception:
            import traceback
            traceback.print_exc()
            self.completed(success=False)
        self.completed(success=True)

    def send_metrics(self, status, combined_data):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        try:
            report_status = {"status": {"status": status},
                             "rows": combined_data.shape[0],
                             "columns": combined_data.shape[1]
                             }
            self.report_status(status=report_status)
        except Exception:
            import traceback
            traceback.print_exc()

    def preprocess(self, data):
        combined_data = pd.merge(data,
                                 self.store_data,
                                 on="Store")
        combined_data.fillna(0, inplace=True)
        logging.info(combined_data.isnull().values.any())
        logging.info(combined_data.head(5))
        self.send_metrics(status="Merged the data", combined_data=combined_data)

        self.prepare_date(data=combined_data)
        logging.info(combined_data.head(5))
        self.send_metrics(status="Data cleaning done", combined_data=combined_data)

        median_cd = combined_data['CompetitionDistance'].median(
            skipna=True)
        combined_data[
            'CompetitionDistance'] = combined_data.CompetitionDistance.mask(
            combined_data.CompetitionDistance == 0, median_cd)
        combined_data = combined_data[combined_data["Open"] != 0]
        logging.info(combined_data.head(5))
        self.send_metrics(status="Masking the data", combined_data=combined_data)

        # Removing Outlier
        q1 = combined_data['CompetitionDistance'].quantile(0.25)
        q3 = combined_data['CompetitionDistance'].quantile(0.75)
        iqr = q3 - q1
        max_value = q3 + 1.5 * iqr
        combined_data = self.remove_outliers(
            df=combined_data, column='CompetitionDistance',
            min_val=0, max_val=max_value)
        logging.info(combined_data.head(5))
        self.send_metrics(status="Removing outliers", combined_data=combined_data)

        combined_data.drop(["Open", "Date"], axis=1, inplace=True)
        equiv = {'a': 1, 'b': 2, 'c': 3}
        combined_data["NewAssortment"] = combined_data["Assortment"].map(equiv)

        equiv1 = {'a': 1, 'b': 2, 'c': 3, 'd': 4}
        combined_data["NewStoreType"] = combined_data["StoreType"].map(equiv1)
        logging.info(combined_data.head(5))
        self.send_metrics(status="Converting assortment to inte",
                          combined_data=combined_data)
        return combined_data

    def completed(self, push_exp=False, success=True):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
            success: Use to handle failure cases

        """
        try:
            output_dir = "/data/combined_dataset"
            if not os.path.exists(output_dir):
                os.makedirs(output_dir)
            self.combined_train_data.to_csv(os.path.join(output_dir,
                                                         "combined_train.csv"),
                                            index=False)
            self.combined_test_data.to_csv(
                os.path.join(output_dir, "combined_test.csv"),
                index=False)
            logging.info("Data saved")
            super().completed(push_exp=False)
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)


if __name__ == "__main__":
    # To run locally. Use following command:
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/main.py

    data_prep = XgboostDataPrep(train_file_path="/data/combined_dataset/train.csv",
                                test_file_path="/data/combined_dataset/test.csv",
                                store_file_path="/data/combined_dataset/store.csv")
    if len(sys.argv) >= 2:
        data_prep.start(run_name=sys.argv[1])
    else:
        data_prep.start(run_name="")
